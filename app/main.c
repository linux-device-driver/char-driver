#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int printstring(char a[],int n)  {

	
	for(int i=0;i<n;i++)  {
		printf("%c",a[i]);
	}

	printf("\n");
	return 0;
}

int main(void)  {

	char buffer[10],buffer1[10];
	int fd,ch,ret,ch1,len;
	int offset;
	char ch2;

	fd=open("char_dev",O_RDWR);
	if(fd<0)  {
		perror("open");
		exit(EXIT_FAILURE);
	}

	printf("file descriptor is : %d \n",fd);
	lseek(fd,SEEK_SET,0);
	do { 
	printf("Enter the choice : \n");
	printf("1. Write \n");
	printf("2. Read \n");
	printf("3. Seek \n");

	scanf("%d",&ch);
		switch(ch)  {
		
			case 1:
				memset(buffer,'\0',sizeof(char)*10);
				printf("Enter the string : \n");
				scanf("%s",buffer);
				len=strlen(buffer);
				printf("size of the buffer : %d \n",len);
				ret=write(fd,buffer,sizeof(char)*strlen(buffer));
				if(ret<0)  {
					perror("write");
					exit(EXIT_FAILURE);
				}
	
				break;

			case 2: 
				memset(buffer1,'\0',sizeof(char)*10);
				ret=read(fd,buffer1,sizeof(char)*len);
				if(ret<0)  {
					perror("read");
					exit(EXIT_FAILURE);
				}
				printstring(buffer1,10);
				break;

			case 3:
				printf(" 0. SEEK_SET \n 1. SEEK_CUR \n 2. SEEK_END \n");
				scanf("%d",&ch1);
				printf("offset \n");
				scanf("%d",&offset);
				printf("current offset : %ld \n",lseek(fd,ch1,offset));
				break;
		}

		printf("Do you want to continue (y/n) \n");
		getchar();
		scanf("%c",&ch2);

	}while((ch2 == 'Y') || (ch2 == 'y'));
	
	

	close(fd);
	return 0;
}
