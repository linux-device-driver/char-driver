#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#ifndef IOCTL
#define IOCTL

#define MAGIC_NO 'K'
#define RESET 0x01
#define GET_SIZE 0x02

#define DEVICE_RESET _IO(MAGIC_NO,RESET)
#define DEVICE_GETSIZE _IOR(MAGIC_NO,GET_SIZE,unsigned int)

#endif

int main(void)  {

	char ch1;
	int fd,ch;

	unsigned long size=0;
	
	fd=open("char_dev",O_RDWR);
	if(fd == -1)  {
		perror("open");
		exit(EXIT_FAILURE);
	}

	do {
		printf("1. Devic Reset \n");
		printf("2. Get device size \n");
		scanf("%d",&ch);
		
		switch(ch)  {
			case 1:
				ioctl(fd,DEVICE_RESET);
				printf("Device is reset\n");
				break;

			case 2:
				ioctl(fd,DEVICE_GETSIZE,&size);
				printf("Device size is : %ld \n",size);
				break;
		}
		getchar();
		printf("Do you want to continue(y/n) \n");
		scanf("%c",&ch1);
	}while((ch1 == 'y') || (ch1 == 'Y'));

	return 0;
}
