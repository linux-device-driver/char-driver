A sample character device driver on kernel 5.4.0-58.

sample application is also provided to test the driver.

to run application do :
    `cd app
    ./test.sh $MAJORNO ${APP_NAME}
    `
Here ,
    
- **MAJORNO** :- Major number provided to the driver by the kernel.(can check dmesg for value).
- **APP_NAME**- :- Can choose application to run (either main.c or ioctl.c ).
    
