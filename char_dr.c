#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/wait.h>
#include <linux/poll.h>

MODULE_AUTHOR("Vijay");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Test Driver");


/* --------------------------------MACROS Defination -------------------------------------- */

#ifndef DEV_NAME 
#define DEV_NAME	"CHAR_DEV"
#endif

#ifndef DEVICES
#define DEVICES	1
#endif

#ifndef IOCTL
#define IOCTL

#define MAGIC_NO 'K'
#define RESET 0x01
#define GET_SIZE 0x02

#define DEVICE_RESET _IO(MAGIC_NO,RESET)
#define DEVICE_GETSIZE _IOR(MAGIC_NO,GET_SIZE,unsigned int)

#endif



/* ----------------------------------------------------------------------------------------- */

/* ------------------------------- DEVICE STRUCTURE ---------------------------------------- */

struct ch_device  {
	char data[100];
	dev_t device;
	struct cdev cdev;
	struct file_operations fops;
	int total_size;
	int space;
	int pos;

};

/* ----------------------------------------------------------------------------------------- */


/* ------------------------------- FILE OPERATIONS ----------------------------------------- */

int opendev(struct inode *idev, struct file *file)  {


	struct ch_device *device;
	
	#ifndef DEBUG
	printk(KERN_INFO "Begin : %s \n",__func__);
	#endif

	device=container_of(idev->i_cdev,struct ch_device,cdev);
	if(!device)  {
		printk(KERN_ERR "container_of failed \n");
		return -EIO;
	}

	file->private_data=device;
	

	#ifndef DEBUG
	printk(KERN_INFO "End : %s \n",__func__);
	#endif
	return 0;
}

int closedev(struct inode *inode, struct file *file)  {

	#ifndef DEBUG
	printk(KERN_INFO "Begin : %s \n",__func__);
	#endif
	#ifndef DEBUG
	printk(KERN_INFO "End : %s \n",__func__);
	#endif
	return 0;
}


ssize_t writedev(struct file *file, const char __user *buffer, size_t size, loff_t *offset)  {
	
	struct ch_device *device;
	#ifndef DEBUG
	printk(KERN_INFO "Begin : %s \n",__func__);
	#endif


	device=(struct ch_device*)file->private_data;
	if(!device)  {
		printk(KERN_ERR "No device available \n");
		return -EIO;
	}


	if(*offset>device->total_size)  {
		printk(KERN_ERR "No space left on device");
		return -ENOSPC;
	}

	if(size+*offset>device->total_size)
		size=device->total_size - *offset;

	memset(device->data,'\0',sizeof(char)*100);

	if(copy_from_user(device->data + file->f_pos ,buffer,size) != 0)  {
		printk("unable to copy data\n");
		return -EFAULT;
	}
	
	*offset+=(loff_t)size;

	printk(KERN_INFO "offset is : %lld \n",*offset);

	file->f_pos+=*offset;

	#ifndef DEBUG
	printk(KERN_INFO "End : %s \n",__func__);
	#endif

	
	return size;
}


ssize_t readdev(struct file *file, char __user *buffer, size_t size,loff_t *offset)  {
	
	int ret;
	struct ch_device *device;
	#ifndef DEBUG
	printk(KERN_INFO "Begin : %s \n",__func__);
	#endif

	device=(struct ch_device*)file->private_data;
	if(!device)  {
		printk(KERN_INFO "Device not available \n");
		return -EIO;
	}

	if(*offset> device->total_size)
		return 0;

	if(*offset + size > device->total_size)
		size=device->total_size - *offset;
	

	ret=copy_to_user(buffer,(char*)(device->data + file->f_pos),size);
	if(ret)
		return -EFAULT;

	printk(KERN_INFO "file pos is : %lld \n",file->f_pos);
	printk(KERN_INFO "data copied is : %s \n",(device->data + file->f_pos));

	#ifndef DEBUG
	printk(KERN_INFO "End : %s \n",__func__);
	#endif

	*offset+=size;

	file->f_pos+=*offset;


	return size;
}

loff_t seekdev(struct file *file, loff_t offset, int pos)  {
	

	struct ch_device *device;
	#ifndef DEBUG
	printk(KERN_INFO "Begin : %s \n",__func__);
	#endif

	device=(struct ch_device*)file->private_data;
	if(!device)  {
		printk(KERN_ERR "Device does not exist \n");
		return -EIO;
	}

	printk(KERN_INFO "offset : %lld \npos :  %d \n",offset,pos);
	printk(KERN_INFO "FILE POS : %lld \n",file->f_pos);

	switch(offset)  {

		case 0:
			printk(KERN_INFO "SEEK SET operation \n");
			file->f_pos=(loff_t)pos;
			break;

		case 1:
			printk(KERN_INFO "SEEK CUR operation \n");
			file->f_pos+=(loff_t)pos;
			break;

		case 2:
			printk(KERN_INFO "SEEK END operation \n");
			file->f_pos+=(loff_t)(device->total_size + pos);
			if(file->f_pos > device->total_size)  {
				printk(KERN_INFO "offset exceeds device size\n");
				return -ENOSPC;
			}
			break;

		default:
			printk(KERN_INFO "Option invalid \n");
			return -EINVAL;
	}

	printk(KERN_INFO "===> FILE POS : %lld <===\n",file->f_pos);

	return file->f_pos;
	#ifndef DEBUG
	printk(KERN_INFO "End : %s \n",__func__);
	#endif

}

static long ioctldev(struct file *file, unsigned int cmd, unsigned long arg)  {

	int ret;
	struct ch_device *device;

	device=(struct ch_device*)file->private_data;
	if(!device)  {
		printk(KERN_INFO "device invalid \n");
		return -EIO;
	}

	switch(cmd)  {
		
		case DEVICE_RESET:
				memset(device->data,'\0',sizeof(char)*10);
				file->f_pos=0;
				printk(KERN_INFO "Device is reset\n");
				break;
		case DEVICE_GETSIZE:
				printk(KERN_INFO "device->total_size : %d \n",device->total_size);
				ret=copy_to_user((int*)arg,&device->total_size,sizeof(int));
				if(ret != 0)  {
					printk(KERN_ERR "ioctl copy_to_user failed\n");
					return -EINVAL;
				}
				printk(KERN_INFO "Device size returned to user \n");
				break;
	}
	return 0;
}

/* ----------------------------------------------------------------------------------------- */

struct ch_device *device;

static int __init init_func(void)  {
	
	#ifndef DEBUG
	printk("Begin : %s \n",__func__);
	#endif

	device=(struct ch_device*)kmalloc(sizeof(struct device),GFP_KERNEL);
	if(!device)  {
		printk("device kmalloc failed \n");
		return -EIO;
	}
	
	if(alloc_chrdev_region(&device->device,0,DEVICES,DEV_NAME) < 0)  {
		printk(KERN_ERR "alloc_chrdev_failed \n");
		return -EIO;
	}

	device->total_size=10;
	device->space=0;
	device->pos=0;

	printk(KERN_INFO "Major number of device is : %d \n",MAJOR(device->device));
	
	device->fops.open=opendev;
	device->fops.release=closedev;
	device->fops.write=writedev;
	device->fops.read=readdev;
	device->fops.llseek=seekdev;
	device->fops.unlocked_ioctl=ioctldev;

	cdev_init(&device->cdev,&device->fops);

	if(cdev_add(&device->cdev,device->device,1) < 0)  {
		printk(KERN_ERR  "cdev_add failed \n");
		return -EIO;
	}

	#ifndef DEBUG
	printk("End : %s \n",__func__);
	#endif

	return 0;
}

static void __exit clean_func(void)  {
	
	#ifndef DEBUG
	printk("Begin : %s \n",__func__);
	#endif



	cdev_del(&device->cdev);
	unregister_chrdev_region(device->device,1);
	kfree(device);

	#ifndef DEBUG
	printk("End : %s \n",__func__);
	#endif
}

module_init(init_func);
module_exit(clean_func);
